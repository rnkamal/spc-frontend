import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProcessSettingsRoutingModule } from './process-settings-routing.module';
import { ProcessSettingsComponent } from './process-settings.component';
import { SharedModule } from '../../shared/shared.module';
import { NewParameterPopupComponent } from './new-parameter-popup/new-parameter-popup.component';
import { CombinationsReferenceComponent } from './combinations-reference/combinations-reference.component';
import { CombinationTableComponent } from './combination-table/combination-table.component';
import { RevisionTableComponent } from './revision-table/revision-table.component';


@NgModule({
  declarations: [
    ProcessSettingsComponent,
    NewParameterPopupComponent,
    CombinationsReferenceComponent,
    CombinationTableComponent,
    RevisionTableComponent,
  ],
  imports: [
    CommonModule,
    ProcessSettingsRoutingModule,
    SharedModule
  ]
})
export class ProcessSettingsModule { }
