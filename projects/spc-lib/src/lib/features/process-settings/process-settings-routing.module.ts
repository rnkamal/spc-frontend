import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CombinationsReferenceComponent } from './combinations-reference/combinations-reference.component';
import { ProcessSettingsComponent } from './process-settings.component';

const routes: Routes = [
  {
    path: 'combination/:id',
    component: CombinationsReferenceComponent
  },
  {
    path: '',
    component: ProcessSettingsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProcessSettingsRoutingModule { }
