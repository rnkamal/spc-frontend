import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ProcessSettingsService } from 'projects/spc-lib/src/lib/services/process-settings.service';
import { Combination, CombinationRequest } from 'projects/spc-lib/src/lib/models/combination.model';
import { DropDownFilter } from '../../../models/drop-down-filter.model';
import { Router } from '@angular/router';
import { environment } from 'projects/spc-sandbox/src/environments/environment';
import { Subscription } from 'rxjs';

@Component({
  selector: 'spc-combination-table',
  templateUrl: './combination-table.component.html',
  styleUrls: ['./combination-table.component.scss']
})
export class CombinationTableComponent implements OnInit, OnDestroy {

  headerHeight = "50px";
  siteList : DropDownFilter[] = [];
  site: DropDownFilter;
  subscriptionSite: Subscription;
  functionalLocation: DropDownFilter;
  functionalLocationList: DropDownFilter[] = []; 
  subscriptionFunctionalLocation: Subscription;
  combinationDisplayedColumns: string[];
  combinationDataSource: MatTableDataSource<Combination> = new MatTableDataSource<Combination>();
  subscriptionCombination: Subscription;

  constructor(private procSettService: ProcessSettingsService,private router: Router) { }

  ngOnInit(): void {
    this.getSiteList();
  }

  getSiteList(){ 
    this.subscriptionSite = this.procSettService.getSiteList().subscribe((res : DropDownFilter[])=>{
      this.siteList = res
      this.site = res[0]
      this.getFunctionalList(this.site.id)
    })
  }

  getFunctionalList(siteId: string){ 
    this.subscriptionFunctionalLocation = this.procSettService.getFuntionalList(siteId).subscribe((res: DropDownFilter[])=>{
      this.functionalLocationList = res
        if(this.functionalLocation == undefined) { 
          this.functionalLocation = res[0]
          this.getCombinationList(siteId, this.functionalLocation.id) 
        } 
    })
  }

  getCombinationList(siteId: string, functionalLocationId: string){ 
    let combinationRequestObj = {} as CombinationRequest;
    combinationRequestObj.site = [siteId]
    combinationRequestObj.functionalLocation = [functionalLocationId]
    this.subscriptionCombination = this.procSettService.getCombinationList(combinationRequestObj).subscribe((res: Combination)=>{ 
      this.combinationDisplayedColumns = res.attributes
      this.combinationDisplayedColumns.push('action')
      this.combinationDataSource.data = res.combinationResponse
    })
  }

  onSelectionChangeSite(site: DropDownFilter){ 
    this.site = site
    this.getFunctionalList(site.id)
  }

  onSelectionChangeFunctionalLocation(functionalLocation: DropDownFilter){ 
    this.functionalLocation = functionalLocation
    this.getCombinationList(this.site.id, functionalLocation.id)
  }

  showCombinationReference(id : number){
    this.router.navigateByUrl(`${environment.applicationUrl}/process-settings/combination/${id}`);
  }

  ngOnDestroy(): void {
    if(this.subscriptionSite !== undefined) { this.subscriptionSite.unsubscribe() }
    if(this.subscriptionFunctionalLocation !== undefined) { this.subscriptionFunctionalLocation.unsubscribe() }
    if(this.subscriptionCombination !== undefined) { this.subscriptionCombination.unsubscribe() }
  }

}
