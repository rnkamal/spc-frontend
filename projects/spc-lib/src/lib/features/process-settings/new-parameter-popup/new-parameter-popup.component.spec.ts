import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewParameterPopupComponent } from './new-parameter-popup.component';

describe('NewParameterPopupComponent', () => {
  let component: NewParameterPopupComponent;
  let fixture: ComponentFixture<NewParameterPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewParameterPopupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewParameterPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
