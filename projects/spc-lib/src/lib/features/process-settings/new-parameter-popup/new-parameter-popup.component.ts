import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatDialogCloseEvent } from '../../../const';

@Component({
  selector: 'spc-new-parameter-popup',
  templateUrl: './new-parameter-popup.component.html',
  styleUrls: ['./new-parameter-popup.component.scss']
})
export class NewParameterPopupComponent implements OnInit {

  constructor(private dialogRef: MatDialogRef<NewParameterPopupComponent>
  ) { }

  ngOnInit(): void {
  }

  onSave() {
    // on success
    this.dialogRef.close({ event: MatDialogCloseEvent.SUCCESS });
    //on error
    // this.dialogRef.close({ event: MatDialogCloseEvent.FAILED });
  };
  onCancel() {
    //on cancel
    this.dialogRef.close({ event: MatDialogCloseEvent.CANCELLED });
  }

}
