import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { TranslateService } from '@ngx-translate/core';
import { MatDialogCloseEvent } from '../../const';
import { Parameter } from '../../models/parameter.model';
import { ProcessSettingsService } from '../../services/process-settings.service';
import { ToasterService } from '../../services/toaster.service';
import { NewParameterPopupComponent } from './new-parameter-popup/new-parameter-popup.component';

@Component({
  selector: 'spc-process-settings',
  templateUrl: './process-settings.component.html',
  styleUrls: ['./process-settings.component.scss']
})
export class ProcessSettingsComponent implements OnInit, OnDestroy {

  headerHeight = "50px";
  paramDisplayedColumns: string[] = ['tagName', 'description', 'uom', 'group', 'machine', 'cod', 'stdValue'];
  paramDataSource: MatTableDataSource<Parameter> = new MatTableDataSource<Parameter>();
  
  constructor(private procSettService: ProcessSettingsService, private dialog: MatDialog, private toasterService: ToasterService, private translate: TranslateService
  ) { }

  ngOnInit(): void {
    this.paramDataSource.data = this.procSettService.getParameter();
  }

  openNewParamPopup() {
    let dialogRef = this.dialog.open(NewParameterPopupComponent, {
      width: "30em",
      height: "auto",
      disableClose: true,
      backdropClass: "backdrop-margin"
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result?.event == MatDialogCloseEvent.SUCCESS) {
        //refresh the data
      } else if (result?.event === MatDialogCloseEvent.FAILED) {
        //show error message
        this.toasterService.error(this.translate.instant('Base_Layout.Error'));
      }
    });
  }

  ngOnDestroy() {
    this.dialog.closeAll();
  }
}
