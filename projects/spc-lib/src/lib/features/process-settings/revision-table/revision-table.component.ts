import { HttpResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { TranslateService } from '@ngx-translate/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToasterService } from 'projects/spc-lib/src/public-api';
import { MatDialogCloseEvent } from '../../../const';
import { RevisionActive, RevisionParam } from '../../../models/revision.model';
import { ProcessSettingsService } from '../../../services/process-settings.service';
import { RevisionPopupComponent } from '../../../shared/components/revision-popup/revision-popup.component';

@Component({
  selector: 'spc-revision-table',
  templateUrl: './revision-table.component.html',
  styleUrls: ['./revision-table.component.scss']
})
export class RevisionTableComponent implements OnInit, OnDestroy {

  @Input() revision: RevisionActive;
  @Input() isActivate: boolean;
  @Output() updateRevisionActivated = new EventEmitter<string>();
  @Output() updateRevisionTable = new EventEmitter<string>();

  revisionDisplayedColumns: string[] = ['processVariable', 'uom', 'stdValue', 'lsl', 'usl', 'controlType', 'graph', 'action'];
  revisionDataSource: MatTableDataSource<RevisionParam> = new MatTableDataSource<RevisionParam>();
  
  constructor(private dialog: MatDialog, private toasterService: ToasterService, private translate: TranslateService) { }

  ngOnInit(): void { 
    this.revisionDataSource.data = this.revision.revisionParam
  }

  onClickRevisionActivate(combinationRevisionId: string) {
    this.updateRevisionActivated.emit(combinationRevisionId)
  }

  openRevisionPopup(element: any) {
    let dialogRef = this.dialog.open(RevisionPopupComponent, {
      width: "35em",
      height: "auto",
      disableClose: true,
      backdropClass: "backdrop-margin",
      data: element
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result?.event == MatDialogCloseEvent.SUCCESS) {
        //refresh the data
        this.updateRevisionTable.emit();
      } else if (result?.event === MatDialogCloseEvent.FAILED) {
        //show error message
        this.toasterService.error(this.translate.instant('Base_Layout.Error'));
      }
    });
  }

  ngOnDestroy() {
    this.dialog.closeAll();
  }

}
