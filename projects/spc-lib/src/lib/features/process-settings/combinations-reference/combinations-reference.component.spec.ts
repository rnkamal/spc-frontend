import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CombinationsReferenceComponent } from './combinations-reference.component';

describe('CombinationsReferenceComponent', () => {
  let component: CombinationsReferenceComponent;
  let fixture: ComponentFixture<CombinationsReferenceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CombinationsReferenceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CombinationsReferenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
