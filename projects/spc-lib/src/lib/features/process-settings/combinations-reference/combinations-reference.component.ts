import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'projects/spc-sandbox/src/environments/environment';
import { Combination } from '../../../models/combination.model';
import { Subscription } from 'rxjs';
import { Revision, RevisionActive, RevisionParam } from '../../../models/revision.model';
import { ProcessSettingsService } from '../../../services/process-settings.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'spc-combinations-reference',
  templateUrl: './combinations-reference.component.html',
  styleUrls: ['./combinations-reference.component.scss']
})
export class CombinationsReferenceComponent implements OnInit, OnDestroy {

  headerHeight = "50px";
  combination : Combination = {} as Combination;
  subscriptionCombinationDetails: Subscription;
  combinationId: string;
  subscriptionParams: Subscription;
  revisionActive: RevisionActive = {} as RevisionActive;
  subscriptionRevisionActive: Subscription;
  revisionAll: Revision[] = [];
  subscriptionRevisionAll: Subscription;
  subscriptionRevisionParam: Subscription;

  revision: RevisionActive = {} as RevisionActive;
  tabSelected: number;
  subscriptionActivated: Subscription;

  constructor(private router: Router,private route: ActivatedRoute,private procSettService : ProcessSettingsService,private spinner : NgxSpinnerService) { 
    this.subscriptionParams = this.route.params.subscribe(params => {
      this.combinationId = params['id']
    });
  }

  ngOnInit(): void {
    this.combination.attributes = [];
    this.combination.combinationResponse = [];
    this.getCombinationDetails();
    this.getRevisionActive(); 
  }

  onChangeTab(event: any){ 
    if(event.index == 1 && this.revisionAll.length == 0) {
      this.getRevisionAll()
    } 
  }

  onOpenPanel(revision: Revision) {
    this.getRevisionParam(revision)
  }

  showProcessSettingsView(){
    this.router.navigateByUrl(`${environment.applicationUrl}/process-settings`);
  }

  getCombinationDetails(){ 
    this.subscriptionCombinationDetails = this.procSettService.getCombinationDetails(this.combinationId).subscribe((res: Combination)=>{
      this.combination = res;
    })
  }

  getRevisionActive() {
    this.subscriptionRevisionActive = this.procSettService.getRevisionActive(this.combinationId).subscribe((revisionActive: RevisionActive)=> {
      this.revisionActive = revisionActive
    })
  }

  getRevisionAll() {
    this.subscriptionRevisionAll = this.procSettService.getRevisionAll(this.combinationId).subscribe((revisionAll: Revision[])=> { 
      this.revisionAll = revisionAll
    })
  }

  getRevisionParam(revision: Revision) {
    this.revision = {} as RevisionActive;
    this.spinner.show();
    this.subscriptionRevisionParam = this.procSettService.getRevisionParam(revision.combinationRevisionId).subscribe((revisionParam: RevisionParam[])=> {
      this.spinner.hide();
      let objRevisionActive = {} as RevisionActive;
      Object.assign(objRevisionActive, revision);
      objRevisionActive.revisionParam = revisionParam;
      this.revision = objRevisionActive;
    })
  }

  onRevisionActivated(combinationRevisionId: string) {
    this.subscriptionActivated = this.procSettService.updateRevisionActivate(combinationRevisionId).subscribe(()=> {
      this.getCombinationDetails();
      this.getRevisionActive(); 
      this.getRevisionAll();
    })
  }

  onUpdateRevisionTable(){
    this.revisionActive = {} as RevisionActive;
    this.getRevisionActive(); 
    this.getRevisionAll();
  }

  ngOnDestroy(): void {
    if(this.subscriptionParams !== undefined) { this.subscriptionParams.unsubscribe() }
    if(this.subscriptionCombinationDetails !== undefined) { this.subscriptionCombinationDetails.unsubscribe() }
    if(this.subscriptionRevisionAll !== undefined) { this.subscriptionRevisionAll.unsubscribe() }
    if(this.subscriptionRevisionParam !== undefined) { this.subscriptionRevisionParam.unsubscribe() }
    if(this.subscriptionRevisionActive !== undefined) { this.subscriptionRevisionActive.unsubscribe() }
    if(this.subscriptionActivated !== undefined) { this.subscriptionActivated.unsubscribe() }
  }
  
}
