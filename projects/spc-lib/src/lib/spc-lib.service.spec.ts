import { TestBed } from '@angular/core/testing';

import { SpcLibService } from './spc-lib.service';

describe('SpcLibService', () => {
  let service: SpcLibService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SpcLibService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
