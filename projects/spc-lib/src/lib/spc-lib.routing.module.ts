import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SpcLibComponent } from './spc-lib.component';

const routes: Routes = [
  {
    path: '',
    component: SpcLibComponent,
    children: [
      {
        path: 'process-settings',
        loadChildren: () => import('./features/process-settings/process-settings.module').then((m) => m.ProcessSettingsModule),
      },
      {
        path: '/',
        pathMatch: "full",
        // redirectTo: 'process-settings'
        redirectTo: ''
      },
      {
        path: '**',
        // redirectTo: 'process-settings'
        redirectTo: ''
      },
      {
        path: '',
        pathMatch: "full",
        // redirectTo: 'process-settings'
        redirectTo: ''
      },

    ]
  },
  {
    path: '**',
    redirectTo: ''
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SpcLibRoutingModule { }
