import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpcLibComponent } from './spc-lib.component';

describe('SpcLibComponent', () => {
  let component: SpcLibComponent;
  let fixture: ComponentFixture<SpcLibComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpcLibComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpcLibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
