export interface Revision {
    combinationRevisionId: string,
    revision: string,
    createdBy: string,
    createdOn: string,
    reason: string,
    active: boolean
}

export interface RevisionParam {
    revisionParamId: string,
    parameterId: string,
    spc: boolean,
    processVariable: string,
    uom: string,
    stdValue: string,
    lsl: string,
    usl: string,
    mean: string,
    average: string,
    controlType: string,
    combinationRevisionId: string,
    graph: string,
    graph2Rules: Array<number>,
    graph1Rules: Array<number>
}


export interface RevisionActive {
    combinationRevisionId: string,
    revision: string,
    createdBy: string,
    createdOn: string,
    reason: string,
    active: boolean,
    revisionParam: Array<RevisionParam>
}


export interface CombinationRevisionParams {
    revisionParamId: string,
    parameterId: string,
    spc: boolean,
    processVariable: string,
    uom: string,
    stdValue: string,
    lsl: string,
    usl: string,
    mean: string,
    average: string,
    controlType: string,
    combinationRevisionId: string,
    graph: string,
    graph2Rules: Array<number>,
    graph1Rules: Array<number>
}