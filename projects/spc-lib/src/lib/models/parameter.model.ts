export interface Parameter{
    tagName: string,
    description: string,
    uom: string,
    group: string,
    machine: string,
    cod: string,
    stdValue: string
}