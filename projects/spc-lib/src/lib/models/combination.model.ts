export interface Combination{
    attributes: Array<string>,
    combinationResponse: []
}

export interface CombinationRequest{
    site: Array<string>,
    functionalLocation: Array<string>
}

