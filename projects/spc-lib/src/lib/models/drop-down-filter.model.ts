export interface DropDownFilter{
    id:string,
    description:string
}
export interface GroupedMachineDropDown{
    idModule: string,
    machines:{ id:string, idModule:string }[]
}