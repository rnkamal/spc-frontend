export interface Menu{
    icon: string,
    title: string,
    routing:string
}