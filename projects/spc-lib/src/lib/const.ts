const relativeApiUrl = {
    PROCESS_SETTINGS: "/process-settings",
    USER : "/user",
    COMBINATION: "/combination",
    REVISION: "/revision",
    CONTROL_CHART: "/control-chart",
    GRAPH: "/graph",
    SPC_RULE: "/spc-rule"
}

export const constantvalues = {
    API_URL: {
        //COMBINATION: `${relativeApiUrl.PROCESS_SETTINGS}/load-combination`,
        SITES : `${relativeApiUrl.USER}/sites`,
        COMBINATION : `${relativeApiUrl.COMBINATION}`,
        COMBINATION_LIST: `${relativeApiUrl.COMBINATION}/list`,
        COMBINATION_REVISION: `${relativeApiUrl.COMBINATION}${relativeApiUrl.REVISION}`,
        COMBINANTION_REVISION_CREATE: `${relativeApiUrl.COMBINATION}${relativeApiUrl.REVISION}/create`,
        COMBINATION_REVISION_PARAMS: `${relativeApiUrl.COMBINATION}${relativeApiUrl.REVISION}/params`,
        REVISION: `${relativeApiUrl.REVISION}`,
        REVISION_ALL: `${relativeApiUrl.REVISION}/all`,
        REVISION_ACTIVE_PARAM: `${relativeApiUrl.REVISION}/active/param`,
        CONTROL_CHART: `${relativeApiUrl.CONTROL_CHART}`,
        CONTROL_CHART_ALL: `${relativeApiUrl.CONTROL_CHART}/all`,
        GRAPH_ALL: `${relativeApiUrl.CONTROL_CHART}/all`,
        SPC_RULE_ALL: `${relativeApiUrl.SPC_RULE}/all`,
        UPLOAD_REVISION : `${relativeApiUrl.COMBINATION}/revision/params`
    },
    FUNCTIONAL_LOCATION: "/functional-loc",
    PARAM: "/param",
    ACTIVATE: "/activate",
    UPDATE: "/update",
    GRAPH: "/graph",
    RECALCULATE_CONTROL_LIMIT:'/recalculate-control-limit'
}
export enum MatDialogCloseEvent { SUCCESS = 100, FAILED = 101, CANCELLED = -1 }
