import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RevisionPopupComponent } from './revision-popup.component';

describe('RevisionPopupComponent', () => {
  let component: RevisionPopupComponent;
  let fixture: ComponentFixture<RevisionPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RevisionPopupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RevisionPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
