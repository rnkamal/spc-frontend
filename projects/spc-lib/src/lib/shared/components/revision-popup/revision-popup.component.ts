import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatDialogRef } from '@angular/material/dialog';
import { MatDialogCloseEvent } from '../../../const';
import { ProcessSettingsService } from '../../../services/process-settings.service';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { DropDownFilter } from './../../../models/drop-down-filter.model';
import { CombinationRevisionParams } from '../../../models/revision.model';
import { TranslateService } from '@ngx-translate/core';
import { MatRadioChange } from '@angular/material/radio';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpResponse } from '@angular/common/http';
import { MatTableDataSource } from '@angular/material/table';
import { ToasterService } from '../../../services/toaster.service';

@Component({
  selector: 'spc-revision-popup',
  templateUrl: './revision-popup.component.html',
  styleUrls: ['./revision-popup.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RevisionPopupComponent implements OnInit {

  formGroup: FormGroup = this.formBuilder.group({
    spcStatus: new FormControl('', Validators.required),
    stdValue: new FormControl('', Validators.required),
    lsl: new FormControl('', Validators.required),
    usl: new FormControl('', Validators.required),
    controlChartId: new FormControl('', Validators.required),
    subgroupSize: new FormControl('', Validators.required),
    graphId: new FormControl('', Validators.required),
    calculationType: new FormControl('', Validators.required),
    points: new FormControl("", Validators.required),
  });
  revisionFile : File[];
  radioButtonError: boolean = false;
  controlSubgroupSize: FormControl;
  errorSubgroupSize: string;

  paramsBefore: any;
  first: boolean = true;
  controlChartAll: DropDownFilter[] = [];
  graphAll: DropDownFilter[] = [];
  chosenGraph: DropDownFilter[] = [];
  spcRuleAll: DropDownFilter[] = [];
  pointsDisplayedColumns: string[] = ['point', 'value'];
  pointsDataSource: MatTableDataSource<any> = new MatTableDataSource<any>();
  uploadExcelResponse: any;
  excelId: number;
  selectedRevisionTabIndex : number = 0;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private dialogRef: MatDialogRef<RevisionPopupComponent>, private formBuilder: FormBuilder,
    private procSettService: ProcessSettingsService, private translate: TranslateService, private toasterService: ToasterService,
    private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.getCombinationRevisionParams(this.data)
    this.getControlChartAll()
    this.getSpcRuleAll()
  }

  getCombinationRevisionParams(revisionParamId: string) {
    this.procSettService.getCombinationRevisionParams(revisionParamId).subscribe((res: any) => {
      this.paramsBefore = res;
      this.formGroup.patchValue({
        spcStatus: res.spc,
        stdValue: res.revisionParamDetails.stdValue,
        lsl: res.revisionParamDetails.lsl,
        usl: res.revisionParamDetails.usl,
        controlChartId: res.revisionParamDetails.controlChartTypeId,
        graphId: res.revisionParamDetails.graphId,
        calculationType: res.revisionParamDetails.calculationType,
        points: res.revisionParamDetails.points || 25
      })
      // this.formGroup.addControl('manualStdValue0', this.formBuilder.control('', [Validators.required]))
      // this.formGroup.addControl('manualLcl0', this.formBuilder.control('', [Validators.required]))
      // this.formGroup.addControl('manualUcl0', this.formBuilder.control('', [Validators.required]))
      this.getControlChartGraph(res.revisionParamDetails.controlChartTypeId, false)
    })
  }

  onSelectControlChart(controlChartId: string) {
    this.getControlChartGraph(controlChartId, true)
    this.setSubgroupSize()
  }

  onSelectManualCriteria(event: MatRadioChange) {
    if (event.value == 'automatic') {
      this.chosenGraph.forEach((graph) => {
        this.formGroup.get('lcl' + graph.id)?.setValue(0);
        this.formGroup.get('ucl' + graph.id)?.setValue(0);
        this.formGroup.get('setStdValue' + graph.id)?.setValue(0);
      })
    } else {
      this.chosenGraph.forEach((graph) => {
        if (this.paramsBefore.revisionParamDetails?.controlChartTypeId == this.formGroup.get('controlChartId')?.value) {
          this.paramsBefore.revisionParamDetails.graphDetails.forEach((element: any) => {
            if (graph.id == element.graphId) {
              this.formGroup.get(`setStdValue${graph.id}`)?.setValue(element.mean);
              this.formGroup.get(`lcl${graph.id}`)?.setValue(element.lcl);
              this.formGroup.get(`ucl${graph.id}`)?.setValue(element.ucl);
            }
          });
        }
      })
    }
  }

  getControlChartAll() {
    this.procSettService.getControlChartAll().subscribe((res: DropDownFilter[]) => {
      this.controlChartAll = res;
    })
  }

  setSubgroupSize() {
    this.controlChartAll.map((controlChart: DropDownFilter) => {
      if (this.formGroup.get('controlChartId')?.value == controlChart.id) {
        if (controlChart.description.toUpperCase() == 'I-MR') {
          this.formGroup.get('subgroupSize')?.disable()
          this.formGroup.patchValue({ subgroupSize: 1 })
          this.formGroup.get('subgroupSize')?.setValidators(Validators.required)
        }
        if (controlChart.description.toUpperCase() == 'XBAR-R') {
          this.formGroup.get('subgroupSize')?.enable()
          this.formGroup.get('subgroupSize')?.setValidators([Validators.required, Validators.min(2), Validators.max(8)])
          this.formGroup.patchValue({ subgroupSize: 2 })
          this.errorSubgroupSize = this.translate.instant('Process_Settings.Value_Between_2_8')
        }
        if (controlChart.description.toUpperCase() == 'XBAR-S') {
          this.formGroup.get('subgroupSize')?.enable()
          this.formGroup.get('subgroupSize')?.setValidators([Validators.required, Validators.min(9)])
          this.formGroup.patchValue({ subgroupSize: 9 })
          this.errorSubgroupSize = this.translate.instant('Process_Settings.Value_Greater_9')
        }
      }
    })
  }

  getControlChartGraph(controlChartId: string, onSelectChange: boolean) {
    this.procSettService.getControlChartGraph(controlChartId).subscribe((res: DropDownFilter[]) => {
      this.graphAll = res;
      if (onSelectChange) {
        this.formGroup.get('graphId')?.setValue(res[0].id);
      }
      this.setSubgroupSize();
    })
  }

  getSpcRuleAll() {
    this.procSettService.getSpcRuleAll().subscribe((res: DropDownFilter[]) => {
      this.spcRuleAll = res;
    })
  }

  onNext() {
    if (this.formGroup.get('spcStatus')?.invalid) {
      return this.formGroup.get('spcStatus')?.markAsTouched()
    } else if (this.formGroup.get('stdValue')?.invalid) {
      return this.formGroup.get('stdValue')?.markAsTouched()
    } else if (this.formGroup.get('lsl')?.invalid) {
      return this.formGroup.get('lsl')?.markAsTouched()
    } else if (this.formGroup.get('usl')?.invalid) {
      return this.formGroup.get('usl')?.markAsTouched()
    } else if (this.formGroup.get('controlChartId')?.invalid) {
      return this.formGroup.get('controlChartId')?.markAsTouched()
    } else if (this.formGroup.get('subgroupSize')?.invalid) {
      return this.formGroup.get('subgroupSize')?.markAsTouched()
    } else if (this.formGroup.get('graphId')?.invalid) {
      return
    } else {
      if (this.radioButtonError) this.radioButtonError = false
      this.chosenGraph = [];
      this.first = !this.first
      let chosen = this.formGroup.get('graphId')?.value; console.log(chosen, 'cc')
      // let i=0;
      let graphs = this.graphAll
      this.graphAll.map((graph: DropDownFilter) => {
        if (graph.id === chosen) {
          if (graph.description.includes('-')) {
            let j = 0;
            graphs.map((res) => {
              if (res.id !== graph.id) {
                this.chosenGraph.push(res)
                //this.formGroup.addControl(`setStdValue${j}`, this.formBuilder.control(`manual${j}`))
                //j++
              }
            })
          } else {
            this.chosenGraph.push(graph)
            //this.formGroup.addControl(`setStdValue${i}`, this.formBuilder.control(`manual${i}`))
            //i++
          }
        }
      })
      this.chosenGraph.forEach((graph) => {
        this.formGroup.addControl(`setStdValue${graph.id}`, this.formBuilder.control('', Validators.required));
        this.formGroup.addControl(`lcl${graph.id}`, this.formBuilder.control('', Validators.required));
        this.formGroup.addControl(`ucl${graph.id}`, this.formBuilder.control('', Validators.required));
        this.formGroup.addControl(`spcRules${graph.id}`, this.formBuilder.control('', Validators.required))
        this.paramsBefore.revisionParamDetails.graphDetails.forEach((element: any) => {
          if (graph.id == element.graphId) {
            this.formGroup.get(`setStdValue${graph.id}`)?.setValue(element.mean);
            this.formGroup.get(`lcl${graph.id}`)?.setValue(element.lcl);
            this.formGroup.get(`ucl${graph.id}`)?.setValue(element.ucl);
            this.formGroup.get(`spcRules${graph.id}`)?.setValue(element.graphRules);
          }
        });

      })
      // if(this.formGroup.get('graphId')?.value == this.paramsBefore.graph && this.paramsBefore.graph1Rules && this.formGroup.get('setStdValue0')) {
      //   this.formGroup.addControl('spcRules1', this.formBuilder.control(this.paramsBefore.graph1Rules, Validators.required))
      // } 
      // if(this.formGroup.get('graphId')?.value == this.paramsBefore.graph && this.paramsBefore.graph2Rules && this.formGroup.get('setStdValue1')) {
      //   this.formGroup.addControl('spcRules2', this.formBuilder.control(this.paramsBefore.graph2Rules, Validators.required))
      // }
      console.log(this.formGroup)
    }
  }
  onBack() {
    this.first = !this.first;
    this.uploadExcelResponse = {};
    this.excelId = 0;
    this.pointsDataSource.data = [];
  }
  onSave() {
    if(this.formGroup.get('calculationType')?.value == "upload" && !this.excelId){
      this.toasterService.error("Please upload the file");
      return;
    }
    if (this.formGroup.invalid) {
      this.formGroup.markAllAsTouched();
      this.toasterService.error("Please fill the required fields");
      return
    } else {
      let formValues = this.formGroup.value;
      let objCombinationRevision: any = {};
      objCombinationRevision.revisionParamId = this.paramsBefore.revisionParamId;
      objCombinationRevision.parameterId = this.paramsBefore.parameterId;
      objCombinationRevision.combinationRevisionId = this.paramsBefore.combinationRevisionId;
      objCombinationRevision.spc = formValues.spcStatus;
      objCombinationRevision.revisionParamDetails = {}
      objCombinationRevision.revisionParamDetails.controlChartTypeId = formValues.controlChartId;
      objCombinationRevision.revisionParamDetails.graphId = formValues.graphId;
      objCombinationRevision.revisionParamDetails.uom = this.paramsBefore.revisionParamDetails.uom;
      objCombinationRevision.revisionParamDetails.stdValue = formValues.stdValue;
      objCombinationRevision.revisionParamDetails.lsl = formValues.lsl;
      objCombinationRevision.revisionParamDetails.usl = formValues.usl;
      objCombinationRevision.revisionParamDetails.calculationType = formValues.calculationType;
      objCombinationRevision.revisionParamDetails.points = null;
      if (formValues.calculationType == "automatic") {
        objCombinationRevision.revisionParamDetails.points = formValues.points
      }
      objCombinationRevision.revisionParamDetails.graphDetails = [];
      this.chosenGraph.forEach((graph) => {
        let tempObj: any = {}
        tempObj.graphId = graph.id;
        tempObj.graphRules = formValues['spcRules' + graph.id];
        tempObj.lcl = formValues['lcl' + graph.id];
        tempObj.ucl = formValues['ucl' + graph.id];
        tempObj.mean = formValues['setStdValue' + graph.id];
        // if (formValues.calculationType == "manual") {
        //   tempObj.lcl = formValues['lcl' + graph.id];
        //   tempObj.ucl = formValues['ucl' + graph.id];
        //   tempObj.mean = formValues['setStdValue' + graph.id];
        // } else if (formValues.calculationType == "upload") {
        //   tempObj.lcl = this.uploadExcelResponse['lowerControLimit' + graph.description.toUpperCase()][2];
        //   tempObj.ucl = this.uploadExcelResponse['upperControlLimit' + graph.description.toUpperCase()][2];
        //   tempObj.mean = this.uploadExcelResponse['mean' + graph.description.toUpperCase()];
        // }
        objCombinationRevision.revisionParamDetails.graphDetails.push(tempObj);
      });
      this.spinner.show();
      this.procSettService.updateCombinationRevision(objCombinationRevision).subscribe(() => {
        // on success
        this.spinner.hide();
        this.dialogRef.close({ event: MatDialogCloseEvent.SUCCESS });
      }, error => {
        //on error
        this.dialogRef.close({ event: MatDialogCloseEvent.FAILED });
      })
    }

  };
  onCancel() {
    //on cancel
    this.dialogRef.close({ event: MatDialogCloseEvent.CANCELLED });
  }

  uploadRevsionData(files: File[]) {
    let formData = new FormData();
    let controlChartType = "";
    let subgroupSize = "";
    if (this.formGroup.get('controlChartId')?.value != '1') {
      subgroupSize = this.formGroup.get('subgroupSize')?.value
    }
    this.controlChartAll.forEach((controlChart : any)=>{
      if(this.formGroup.get("controlChartId")?.value == controlChart.id){
        controlChartType = controlChart.description;
      }
    })
    formData.append('file', files[0], files[0].name);
    this.spinner.show();
    this.pointsDataSource.data = [];
    this.procSettService.uplaodExcelRevisionData(formData, controlChartType,subgroupSize).subscribe((event: any) => {
      this.revisionFile= new Array<File>();
      if (event instanceof HttpResponse) {
        this.spinner.hide();
        this.uploadExcelResponse = event.body;
        this.excelId = event.body.excelId;
        this.pointsDataSource.data = event.body[`attribute${this.chosenGraph[this.selectedRevisionTabIndex]?.description.toUpperCase()}`];
        this.bindStdValCriteria();
      }
    },
      error => {
      })
  }

  onGraphTabChanged(res: any) {
    this.selectedRevisionTabIndex = res.index;
    this.pointsDataSource.data = this.uploadExcelResponse[`attribute${this.chosenGraph[res.index]?.description.toUpperCase()}`];
  }

  recalculate(graph: any) {
    let subgroupSize = "";
    let controlChartType = "";
    let payload = this.uploadExcelResponse['attribute' + graph.description.toUpperCase()]
    if (this.formGroup.get('controlChartId')?.value != '1') {
      subgroupSize = this.formGroup.get('subgroupSize')?.value
    }
    this.controlChartAll.forEach((element: any) => {
      if (this.formGroup.get('controlChartId')?.value == element.id) {
        controlChartType = element.description;
      }
    })
    this.pointsDataSource.data = [];
    this.spinner.show();
    this.procSettService.getRecalculateData(controlChartType, this.uploadExcelResponse.excelId, subgroupSize, payload).subscribe((res: any) => {
      this.spinner.hide();
      this.uploadExcelResponse[`attribute${graph.description.toUpperCase()}`] = res[`attribute${graph.description.toUpperCase()}`];
      this.uploadExcelResponse['lowerControLimit' + graph.description.toUpperCase()] = res['lowerControLimit' + graph.description.toUpperCase()];
      this.uploadExcelResponse['upperControlLimit' + graph.description.toUpperCase()] = res['upperControlLimit' + graph.description.toUpperCase()];
      this.uploadExcelResponse['mean' + graph.description.toUpperCase()] = res['mean' + graph.description.toUpperCase()];
      //this.uploadExcelResponse = res;
      //this.uploadExcelResponse.excelId = this.excelId;
      this.pointsDataSource.data = res[`attribute${graph.description.toUpperCase()}`];
      this.bindStdValCriteria();
    })
  }

  bindStdValCriteria() {
    this.chosenGraph.forEach((graph) => {
      this.formGroup.get('lcl' + graph.id)?.setValue(this.uploadExcelResponse['lowerControLimit' + graph.description.toUpperCase()][2]);
      this.formGroup.get('ucl' + graph.id)?.setValue(this.uploadExcelResponse['upperControlLimit' + graph.description.toUpperCase()][2]);
      this.formGroup.get('setStdValue' + graph.id)?.setValue(this.uploadExcelResponse['mean' + graph.description.toUpperCase()]);
    })
  }
}
