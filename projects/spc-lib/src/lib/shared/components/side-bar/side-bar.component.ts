import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { environment } from 'projects/spc-sandbox/src/environments/environment';
import { filter } from 'rxjs/operators';
import { Alert } from '../../../models/alert.model';
import { Menu } from '../../../models/menu.model';

@Component({
  selector: 'spc-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent implements OnInit {
  menu: Menu[];
  alert: Alert[];
  appUrl : string = environment.applicationUrl;
  currentRoute: string;
  constructor(private router: Router) { }

  ngOnInit(): void {
    this.currentRoute = this.router.url;
    this.router.events
      .pipe(filter((e: any): e is RouterEvent => e instanceof NavigationEnd))
      .subscribe(() => {
        this.currentRoute = this.router.url;
      });
    this.menu = [{
      icon: 'home',
      title: 'Base_Layout.Home',
      routing:'dashboard'
    },
    {
      icon: 'dvr',
      title: 'Base_Layout.Real_Time_Monitoring',
      routing:'real-time-monitoring'
    },
    {
      icon: 'settings_applications',
      title: 'Base_Layout.Process_Settings',
      routing:'process-settings'
    },
    {
      icon: 'list',
      title: 'Base_Layout.SPC_Logs',
      routing:'spc-logs'
    }, {
      icon: 'account_circle',
      title: 'Base_Layout.Roles_Auth',
      routing:'roles'
    }];
    this.alert =[{
      title:'Title 1',
      date: new Date().toString()
    },
    {
      title:'Title 2',
      date: new Date().toString()
    },{
      title:'Title 3',
      date: new Date().toString()
    }]
  }

}
