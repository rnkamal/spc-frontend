import { Injectable, EventEmitter } from '@angular/core';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { EnvService } from '../services/env.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { constantvalues } from '../const';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  public gridpagesize=new BehaviorSubject<number>(0);
  private dataSource = new BehaviorSubject<string>("");
  title: Observable<any> = this.dataSource.asObservable();
  private workstation = new BehaviorSubject<string>("");
  selectedworkstation: Observable<any> = this.workstation.asObservable();
  private routes = new BehaviorSubject<string>("");
  subRoutes: Observable<any> = this.routes.asObservable()
  private module: string = "";
  invokeEvents = new EventEmitter();
  baseURL : string = "";
  constructor(private hc: HttpClient, private env: EnvService, private dialog: MatDialog, private router: Router) { 
    this.env.envSubject$.subscribe((res : any) => { 
      this.baseURL = res.apiUrl 
    });
  }
  

  setPageTitle(moduleName: string) {
    this.dataSource.next(moduleName)
  }

  setSubRoutes(obj: any) {
    this.routes.next(obj)
  }

  logout() {
    this.env.currentApplication = "";
    //this.oauthService.logOut();
    localStorage.clear();
    this.router.navigateByUrl('/login');
  }
  
  
}
