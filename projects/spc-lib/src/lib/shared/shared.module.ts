import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './modules/material.module';
import { SideBarComponent } from './components/side-bar/side-bar.component';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { RouterModule } from '@angular/router';
import { RevisionPopupComponent } from './components/revision-popup/revision-popup.component';
import { ngfModule } from "angular-file";
@NgModule({
  declarations: [
    SideBarComponent,
    RevisionPopupComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    TranslateModule,
    FlexLayoutModule,
    RouterModule,
    ngfModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports:[
    SideBarComponent,
    MaterialModule,
    TranslateModule,
    FlexLayoutModule,
    RevisionPopupComponent,
    ngfModule
  ]
})
export class SharedModule { }
