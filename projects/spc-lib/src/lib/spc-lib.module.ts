import { NgModule } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { SharedModule } from './shared/shared.module';
import { SpcLibComponent } from './spc-lib.component';
import { SpcLibRoutingModule } from './spc-lib.routing.module';

@NgModule({
  declarations: [
    SpcLibComponent
  ],
  imports: [
    SpcLibRoutingModule,
    SharedModule
  ],
  exports: [
    SpcLibComponent
  ]
})
export class SpcLibModule { 
  constructor(translate: TranslateService) {
    translate.setDefaultLang('en');
    let browserLang = navigator.language ? navigator.language.substr(0,2) : 'en' ;
    translate.use(browserLang);
  }
}
