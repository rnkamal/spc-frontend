import { Injectable } from '@angular/core';
import { MatSnackBar } from "@angular/material/snack-bar";

@Injectable({
  providedIn: 'root'
})
export class ToasterService {

  constructor(private snackBar: MatSnackBar) { }

  warning(message: string) {
    this.message(message, '', 5000, 'snackbar-warning');
  }

  error(message: string) {
    this.message(message, '', 4000, 'snackbar-error');
  }

  success(message: string) {
    this.message(message, '', 3000, 'snackbar-success');
  }
  message(message: string, action = '', duration = 5000, cssClass = 'snackbar-message') {
    this.snackBar.open(message, action, {
      duration,
      panelClass: [cssClass],
      verticalPosition: 'top'
    });
  }
  close(){
    this.snackBar.dismiss();
  }
  open(message: string,action = '',cssClass = 'snackbar-message'){
    this.snackBar.open(message, action, {
      panelClass: [cssClass],
      verticalPosition: 'top'
    });
  }
}
