import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EnvService {
  // API url
  public apiUrl = '';
  public signalRUrl = '';
  public signalRConnectionTryCount = 0;
  public authEndPointURL = '';
  public authUserEndPointURL = '';
  public authScope = '';
  public authClientId = '';
  public authDummyClientSecret = '';
  public authIssuer = '';
  public environmentName = '';
  public environmentLabelBGColor = '';
  public currentApplication = "";
  envSubject: BehaviorSubject<any> = new BehaviorSubject("");
  envSubject$: Observable<any> = this.envSubject.asObservable();

  constructor() {
  }

  setEnvProperties(configObj: any) {
    let obj: any = this;
    for (const key in configObj) {
      if (configObj.hasOwnProperty(key)) {
        obj[key] = configObj[key];
      }
    }
    this.envSubject.next(configObj);
  }

}
