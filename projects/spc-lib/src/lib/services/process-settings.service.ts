import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { constantvalues } from "../const";
import { Parameter } from "../models/parameter.model";
import { CombinationRevisionParams } from "../models/revision.model";
import { EnvService } from "./env.service";

@Injectable({
  providedIn: 'root'
})
export class ProcessSettingsService {
  baseURL: string;
  constructor(private httpClient: HttpClient, private env: EnvService) {
    this.env.envSubject$.subscribe((res: any) => {
      this.baseURL = res.apiUrl
    });
  }


  getSiteList() {
    return this.httpClient.get(`${this.baseURL}${constantvalues.API_URL.SITES}`);
  }

  getFuntionalList(siteId: string) {
    return this.httpClient.get(`${this.baseURL}${constantvalues.API_URL.SITES}/${siteId}${constantvalues.FUNCTIONAL_LOCATION}`);
  }

  getCombinationDetails(combinationId: string) {
    return this.httpClient.get(`${this.baseURL}${constantvalues.API_URL.COMBINATION}/${combinationId}`);
  }

  getCombinationList(obj: object) {
    return this.httpClient.post(`${this.baseURL}${constantvalues.API_URL.COMBINATION_LIST}`, obj)
  }

  getParameter() {
    let data: Parameter[] = [{
      tagName: 'Tag1',
      description: 'Description1',
      uom: 'uom1',
      group: 'Group1',
      machine: 'machine1',
      cod: 'Cod. Approv1',
      stdValue: 'Std. Value1'
    },
    {
      tagName: 'Tag2',
      description: 'Description2',
      uom: 'uom2',
      group: 'Group2',
      machine: 'machine2',
      cod: 'Cod. Approv2',
      stdValue: 'Std. Value2'
    }, {
      tagName: 'Tag3',
      description: 'Description3',
      uom: 'uom3',
      group: 'Group3',
      machine: 'machine3',
      cod: 'Cod. Approv3',
      stdValue: 'Std. Value3'
    }]
    return data;
  }


  getRevisionAll(combinationId: string) {
    return this.httpClient.get(`${this.baseURL}${constantvalues.API_URL.COMBINATION}/${combinationId}${constantvalues.API_URL.REVISION_ALL}`)
  }

  getRevisionParam(combinationRevisionId: string) {
    return this.httpClient.get(`${this.baseURL}${constantvalues.API_URL.COMBINATION_REVISION}/${combinationRevisionId}${constantvalues.PARAM}`)
  }

  getRevisionActive(combinationId: string) {
    return this.httpClient.get(`${this.baseURL}${constantvalues.API_URL.COMBINATION}/${combinationId}${constantvalues.API_URL.REVISION_ACTIVE_PARAM}`)
  }

  updateRevisionActivate(combinationRevisionId: string) {
    return this.httpClient.put(`${this.baseURL}${constantvalues.API_URL.COMBINATION_REVISION}/${combinationRevisionId}${constantvalues.ACTIVATE}`, '')
  }


  getCombinationRevisionParams(revisionParamId: string) {
    return this.httpClient.get(`${this.baseURL}${constantvalues.API_URL.COMBINATION_REVISION_PARAMS}/${revisionParamId}`)
  }

  getControlChartAll() {
    return this.httpClient.get(`${this.baseURL}${constantvalues.API_URL.CONTROL_CHART_ALL}`)
  }

  getControlChartGraph(controlChartId: string) {
    return this.httpClient.get(`${this.baseURL}${constantvalues.API_URL.CONTROL_CHART}/${controlChartId}${constantvalues.GRAPH}`)
  }

  getSpcRuleAll() {
    return this.httpClient.get(`${this.baseURL}${constantvalues.API_URL.SPC_RULE_ALL}`)
  }

  uplaodExcelRevisionData(payload: any, controlChartType: string,subgroupSize : any) {
    let url = "";
    if(subgroupSize){
      url= `${controlChartType}/${subgroupSize}`;
    }else{
      url= `${controlChartType}`;
    }
    return this.httpClient.post(`${this.baseURL}${constantvalues.API_URL.UPLOAD_REVISION}/${url}/upload-excel/control-limit`, payload, {
      reportProgress: true,
      observe: "events"
    });
  }

  updateCombinationRevision(objCombinationRevision: CombinationRevisionParams) {
    return this.httpClient.put(`${this.baseURL}${constantvalues.API_URL.COMBINATION_REVISION_PARAMS}${constantvalues.UPDATE}`, objCombinationRevision)
  }

  getRecalculateData(controlChartType : any,excelId :any,subgroupSize : any,payload :any){
    let url = "";
    if(subgroupSize){
      url= `${controlChartType}/${subgroupSize}/${excelId}`;
    }else{
      url= `${controlChartType}/${excelId}`;
    }
    return this.httpClient.post(`${this.baseURL}${constantvalues.API_URL.COMBINATION_REVISION_PARAMS}/${url}${constantvalues.RECALCULATE_CONTROL_LIMIT}`,payload);
  }

}