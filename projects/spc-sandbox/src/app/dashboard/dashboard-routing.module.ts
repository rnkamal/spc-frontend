import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';

const routes: Routes = [  {
  path: 'spc',
  component: DashboardComponent,
  children: [
    {
      path: '',
      loadChildren: () =>
        import('projects/spc-lib/src/lib/spc-lib.module').then(
          (m) => m.SpcLibModule
        ),
    },
  ],
},
{
  path: '',
  pathMatch: "full",
  redirectTo: 'spc'
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
