import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner'
import { finalize, catchError } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { ToasterService } from 'projects/spc-lib/src/public-api';

@Injectable({
    providedIn: 'root'
})
export class AuthInterceptor implements HttpInterceptor {

    private totalRequests = 0;
    constructor(private spinner: NgxSpinnerService, private toaster: ToasterService, private translateService: TranslateService) {
    }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        // if(request.url.indexOf("10.0.1.9:48080/auth") != -1){
        //     return next.handle(request);
        // }
        // let token = this.oAuthStorage.getItem("access_token");
        // let userInfo = this.oAuthStorage.getItem("id_token_claims_obj");
        // const browserWindow: any = window || {};
        // let module = browserWindow['__env']['moduleName'];
        // let lang = navigator.language.split("-")[0];
        // let version = browserWindow['__env']['factoryVersion'];
        // let ip :any = localStorage.getItem("ipAddress")?.toString();


        // if (token && userInfo && request.url.indexOf("login") === -1) {

        //     request = request.clone({
        //         //url: request.url.replace("http://", "https://"),
        //         setHeaders: {
        //             Authorization: `Bearer ${token}`,
        //             Username: JSON.parse(userInfo).preferred_username,
        //             Module: module,
        //             Lang: lang,
        //             Version: version
        //             // IpAddress : ip
        //         }
        //     });
        // }
        // if(this.totalRequests === 0){
        //     this.spinner.show();
        // }

        // this.totalRequests++;

        return next.handle(request)
            .pipe(
                catchError((error: HttpErrorResponse) => {
                    // this.decreaseRequests();
                    if (error.status === 401 || error.status === 403) {
                        this.spinner.hide();
                        this.toaster.warning(this.translateService.instant("Base_Layout.Unauthorized_api_access"));
                    }
                    else if (error.status !== 409) {
                        this.spinner.hide();
                        this.toaster.warning(this.translateService.instant("Login.ErrorMessage_PageRefresh"));
                    }
                    return throwError(error)
                }),
                finalize(() => {

                    // this.decreaseRequests();
                    // this.spinner.hide();        
                })
            );
    }

    private decreaseRequests() {

        this.totalRequests--;

        if (this.totalRequests === 0) {
            setTimeout(() => {
                this.spinner.hide();
            }, 50)
        }
    }
}
