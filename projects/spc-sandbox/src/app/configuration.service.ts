import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EnvService } from 'projects/spc-lib/src/lib/services/env.service';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {

  _configuration: any;
  constructor(private env: EnvService, private httpClient: HttpClient) { }
  public loadAppConfig() {
    return this.httpClient.get(`./appconfig.json`)
      .pipe(
        tap(response => response),
        tap((response: any) => {
          const browserWindow: any = window || {};
          browserWindow.configuration = response || {};
          let configObj = response[response.currentEnv];
          this.env.setEnvProperties(configObj);
          return this._configuration = response;
        })
      ).toPromise();
  }

}

